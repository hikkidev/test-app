/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.data.storage

class Constants {
    companion object {

        // Tables
        const val TABLE_BOOKS = "table_book"

        // Columns
        const val ID = "id" // in DB
        const val BOOK_ID = "book_id" // ID книги как товара
        const val BOOK_TITLE = "book_title" // Название
        const val BOOK_AUTHORS = "book_authors" // Авторы
    }
}