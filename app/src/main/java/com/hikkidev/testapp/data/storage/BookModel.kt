/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.data.storage

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = Constants.TABLE_BOOKS)
data class BookModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = Constants.ID)
    val id: Int = 0,
    @ColumnInfo(name = Constants.BOOK_ID)
    val productId: Long,
    @ColumnInfo(name = Constants.BOOK_TITLE)
    val title: String,
    @ColumnInfo(name = Constants.BOOK_AUTHORS)
    @field:TypeConverters(Converters::class)
    val author: List<String>
)