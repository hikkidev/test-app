/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.data.storage

import androidx.paging.DataSource
import androidx.room.*
import com.hikkidev.testapp.data.storage.Constants.Companion.BOOK_TITLE
import com.hikkidev.testapp.data.storage.Constants.Companion.ID
import com.hikkidev.testapp.data.storage.Constants.Companion.TABLE_BOOKS

@Dao
interface BookDao {

    @Query("SELECT * from $TABLE_BOOKS WHERE $ID=:id")
    fun getBookById(id: Int): BookModel

    @Query("SELECT * from $TABLE_BOOKS ORDER BY $BOOK_TITLE")
    fun getBooks(): DataSource.Factory<Int, BookModel>

    @Query("SELECT COUNT(*) from $TABLE_BOOKS")
    suspend fun getCountItems(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: BookModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<BookModel>)

    @Update
    suspend fun update(item: BookModel)

    @Update
    suspend fun updateAll(item: List<BookModel>)

    @Delete
    suspend fun delete(item: BookModel)

    @Query("DELETE FROM $TABLE_BOOKS")
    suspend fun deleteAll()
}
