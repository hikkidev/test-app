/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.data.storage

import androidx.room.TypeConverter
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import kotlinx.serialization.serializer

class Converters {
    companion object {
        @JvmStatic
        @TypeConverter
        fun fromList(value: List<String>): String =
            Json(JsonConfiguration.Stable).stringify(String.serializer().list, value)

        @JvmStatic
        @TypeConverter
        fun toList(value: String): List<String> =
            Json(JsonConfiguration.Stable).parse(String.serializer().list, value)
    }
}
