/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.data.storage

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.hikkidev.testapp.domain.gateways.BookGateway
import toothpick.InjectConstructor

@InjectConstructor
class BookGatewayImpl(private val db: AppDatabase) : BookGateway {

    private val dao
        get() = db.getBookDao()
    //Можно добавить кэш и брать от туда

    override suspend fun getBookById(id: Int): BookModel = dao.getBookById(id)

    override fun getBooks(): LiveData<PagedList<BookModel>> {
        val config = PagedList.Config.Builder()
            .setPageSize(10)
            .build()

        return LivePagedListBuilder(
            dao.getBooks(),
            config
        ).build()
    }

    override suspend fun delete(item: BookModel) {
        dao.delete(item)
    }

    override suspend fun update(item: BookModel) {
        dao.update(item)
    }

    override suspend fun insert(item: BookModel) {
        dao.insert(item)
    }

    override suspend fun insertAll(items: List<BookModel>) {
        dao.insertAll(items)
    }

    override suspend fun clearAll() {
        dao.deleteAll()
    }
}