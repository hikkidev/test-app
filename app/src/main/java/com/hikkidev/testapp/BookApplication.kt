/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp

import android.app.Application
import com.hikkidev.testapp.presentation.di.AppModule
import com.hikkidev.testapp.presentation.di.DI
import com.hikkidev.testapp.presentation.di.LogicModule
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration

class BookApplication : Application() {

    private val appScope: Scope get() = Toothpick.openScope(DI.APP_SCOPE)

    override fun onCreate() {
        super.onCreate()
        initToothpick()
        installModules()
    }

    private fun initToothpick() {
        Toothpick.setConfiguration(
            if (BuildConfig.DEBUG) Configuration.forDevelopment().preventMultipleRootScopes()
            else Configuration.forProduction()
        )
    }

    private fun installModules() {
        appScope.installModules(AppModule(this), LogicModule())
    }

}