/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.domain.provider

import com.hikkidev.testapp.data.storage.BookGatewayImpl
import com.hikkidev.testapp.domain.gateways.BookGateway
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class BookGatewayProvider(private val impl: BookGatewayImpl) : Provider<BookGateway> {
    override fun get(): BookGateway = impl
}