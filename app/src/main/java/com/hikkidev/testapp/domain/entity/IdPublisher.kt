/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.domain.entity

interface IdPublisher {
    suspend fun publish(id: Int)
}