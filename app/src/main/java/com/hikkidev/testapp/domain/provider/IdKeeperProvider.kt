/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.domain.provider

import com.hikkidev.testapp.domain.entity.IdKeeper
import com.hikkidev.testapp.domain.implementation.IdHolder
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class IdKeeperProvider(private val impl: IdHolder) : Provider<IdKeeper> {
    override fun get(): IdKeeper = impl
}