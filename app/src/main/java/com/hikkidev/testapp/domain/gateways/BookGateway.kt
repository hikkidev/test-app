/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.domain.gateways

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.hikkidev.testapp.data.storage.BookModel

interface BookGateway {

    fun getBooks(): LiveData<PagedList<BookModel>>

    suspend fun getBookById(id: Int): BookModel

    suspend fun delete(item: BookModel)

    suspend fun update(item: BookModel)

    suspend fun insert(item: BookModel)

    suspend fun insertAll(items: List<BookModel>)

    suspend fun clearAll()
}