/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.domain.implementation

import com.hikkidev.testapp.domain.entity.IdKeeper
import com.hikkidev.testapp.domain.entity.IdPublisher
import kotlinx.coroutines.ExperimentalCoroutinesApi

import kotlinx.coroutines.channels.Channel
import toothpick.InjectConstructor

@ExperimentalCoroutinesApi
@InjectConstructor
class IdHolder : IdKeeper, IdPublisher {
    override val channelState: Channel<Int> = Channel()

    override suspend fun publish(id: Int) {
        channelState.send(id)
    }
}