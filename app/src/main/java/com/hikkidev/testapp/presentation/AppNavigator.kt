/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation

import androidx.fragment.app.FragmentManager
import com.hikkideveloper.core.mvp.navigation.AnimationDescriptor
import com.hikkideveloper.core.mvp.navigation.FragmentNavigator
import com.hikkideveloper.core.mvp.navigation.NavigationDelegate
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppScreen

class AppNavigator(
    delegate: NavigationDelegate,
    fragmentManager: FragmentManager,
    containerId: Int
) : FragmentNavigator(delegate, fragmentManager, containerId, false) {

    private val fade = AnimationDescriptor(
        android.R.anim.fade_in,
        android.R.anim.fade_out,
        android.R.anim.fade_in,
        android.R.anim.fade_out
    )

    override fun create(screen: Screen): FragmentDescriptor {
        return when (screen as SupportAppScreen) {
            Screens.Home -> FragmentDescriptor(screen, fade)
            Screens.DetailBook -> FragmentDescriptor(screen, fade)

            else -> throw RuntimeException("Wrong screen key!")
        }
    }
}
