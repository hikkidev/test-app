/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.home.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkidev.testapp.presentation.common.inflate

class BooksAdapter(private val onClick: (Int) -> Unit) :
    PagedListAdapter<BookModel, BookHolder>(DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookHolder {
        return BookHolder(parent.inflate(R.layout.item_book), ::onItemClick)
    }

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    private fun onItemClick(position: Int) {
        getItem(position)?.let { onClick(it.id) }
    }

    companion object {
        @JvmStatic
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BookModel>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(old: BookModel, new: BookModel): Boolean {
                return old.id == new.id
            }

            override fun areContentsTheSame(old: BookModel, new: BookModel): Boolean {
                return old == new
            }
        }
    }
}