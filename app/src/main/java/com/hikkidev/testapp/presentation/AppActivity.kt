/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hikkidev.testapp.R
import com.hikkidev.testapp.presentation.di.DI
import com.hikkideveloper.core.mvp.activity.BaseActivity
import com.hikkideveloper.core.mvp.navigation.NavigationDelegate
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import com.hikkideveloper.core.mvp.view.BaseView
import toothpick.Toothpick

class AppActivity : BaseActivity(), NavigationDelegate, BaseView {
    override val layoutId = R.layout.activity_main

    private val appScope = Toothpick.openScopes(DI.APP_SCOPE)
    private val navigationManager = appScope.getInstance(NavigationManager::class.java)

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @ProvidePresenter
    fun providePresenter(): AppPresenter = appScope.getInstance(AppPresenter::class.java)

    override fun exit() {
        finish()
    }

    override fun showToast(message: String, duration: Int) {
        Toast.makeText(applicationContext, message, duration).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initScopeAndInject()
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            presenter.coldStart()
        }
    }

    override fun onResume() {
        super.onResume()
        navigationManager.getNavigationHolder(Screens.APP_ROUTER)
            .setNavigator(
                AppNavigator(this, supportFragmentManager, R.id.app_fragment_container)
            )
    }

    override fun onPause() {
        super.onPause()
        navigationManager.getNavigationHolder(Screens.APP_ROUTER).removeNavigator()
    }

    private fun initScopeAndInject() {
        Toothpick.inject(this, appScope)
    }
}
