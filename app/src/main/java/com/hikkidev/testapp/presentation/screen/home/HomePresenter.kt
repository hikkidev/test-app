/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.arellomobile.mvp.InjectViewState
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkidev.testapp.domain.entity.IdPublisher
import com.hikkidev.testapp.domain.gateways.BookGateway
import com.hikkidev.testapp.presentation.Screens
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import com.hikkideveloper.core.mvp.presenter.BasePresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import toothpick.InjectConstructor

@InjectViewState
@InjectConstructor
class HomePresenter(
    private val appContext: Context,
    private val gateway: BookGateway,
    private val idPublisher: IdPublisher,
    navigationManager: NavigationManager
) :
    BasePresenter<HomeView>(navigationManager) {

    private var isFirstAttach = true
    private lateinit var liveList: LiveData<PagedList<BookModel>>

    private val observer = Observer<PagedList<BookModel>> {
        viewState.setBooks(it)
        if (isFirstAttach) {
            viewState.progressBar(false)
            isFirstAttach = false
        }
    }

    fun openDetailsScreen(id: Int) {
        launch(Dispatchers.Default) {
            idPublisher.publish(id)
        }
        getRouter(Screens.APP_ROUTER).navigateTo(Screens.DetailBook)
    }

    fun openNewBookScreen() {
        getRouter(Screens.APP_ROUTER).navigateTo(Screens.DetailBook)
    }

    override fun handleCoroutineException(throwable: Throwable) {
        viewState.showToast(
            throwable.localizedMessage ?: appContext.getString(R.string.internal_error_message)
        )
        throwable.printStackTrace()
    }

    override fun onFirstViewAttach() {
        viewState.progressBar(true)
        liveList = gateway.getBooks()
        liveList.observeForever(observer)
    }

    override fun onDestroy() {
        liveList.removeObserver(observer)
        super.onDestroy()
    }

}