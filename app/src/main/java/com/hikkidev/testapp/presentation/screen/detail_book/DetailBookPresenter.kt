/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.detail_book

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkidev.testapp.domain.entity.IdKeeper
import com.hikkidev.testapp.domain.gateways.BookGateway
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import com.hikkideveloper.core.mvp.presenter.BasePresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import toothpick.InjectConstructor

@InjectViewState
@InjectConstructor
class DetailBookPresenter(
    private val appContext: Context,
    private val idKeeper: IdKeeper,
    private val gateway: BookGateway,
    navigationManager: NavigationManager
) :
    BasePresenter<DetailBookView>(navigationManager) {

    private var bookModel: BookModel? = null

    override fun handleCoroutineException(throwable: Throwable) {
        viewState.showToast(
            throwable.localizedMessage ?: appContext.getString(R.string.internal_error_message)
        )
        throwable.printStackTrace()
    }

    override fun onFirstViewAttach() {
        launch {
            val item = withContext(Dispatchers.Default) {
                val problemId = idKeeper.channelState.receive()
                gateway.getBookById(problemId)
            }
            bookModel = item
            viewState.setInfo(item)
            viewState.setButtons(appContext.getString(R.string.button_update))
        }
    }

    fun removeBook() {
        bookModel?.let {
            launch {
                gateway.delete(it)
                viewState.showMessageDeleted()
            }
        }
    }

    fun handleAction(book: BookModel) {
        val sBook = bookModel
        if (sBook != null) {
            val tmpBook = BookModel(
                sBook.id,
                book.productId,
                book.title,
                book.author
            )
            launch {
                gateway.update(tmpBook)
                viewState.showMessageUpdated()
            }
        } else {
            launch {
                gateway.insert(book)
                viewState.showMessageAdded()
            }
        }
    }

    fun hideDialog() = viewState.hideDialog()
}