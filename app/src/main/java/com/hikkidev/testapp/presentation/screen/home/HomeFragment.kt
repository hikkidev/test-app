/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.home

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout.HORIZONTAL
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkidev.testapp.presentation.Screens
import com.hikkidev.testapp.presentation.common.*
import com.hikkidev.testapp.presentation.screen.home.adapter.BooksAdapter
import com.hikkideveloper.core.mvp.LayoutId
import com.hikkideveloper.core.mvp.fragment.BaseFragment
import com.hikkideveloper.core.mvp.navigation.BackListener
import kotlinx.android.synthetic.main.fragment_home.*
import toothpick.Scope
import toothpick.Toothpick


@LayoutId(R.layout.fragment_home)
class HomeFragment : BaseFragment(), HomeView, BackListener {

    override val scope: Scope = Toothpick.openRootScope()

    private lateinit var adapter: BooksAdapter

    @InjectPresenter
    lateinit var presenter: HomePresenter

    @ProvidePresenter
    fun providePresenter(): HomePresenter = scope.getInstance(
        HomePresenter::class.java
    )

    override fun onBack(): Boolean {
        presenter.back(Screens.APP_ROUTER)
        return true
    }

    override fun progressBar(isEnable: Boolean) {
        if (isEnable) {
            text_info.makeGone()
            content_loader.makeVisible()
        } else {
            text_info.makeVisibleIf { adapter.itemCount == 0 }
            content_loader.makeGone()
        }
    }

    override fun setBooks(list: PagedList<BookModel>) {
        if (list.isEmpty()) {
            text_info.makeVisible()
        } else {
            text_info.makeGone()
        }
        adapter.submitList(list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener { presenter.openNewBookScreen() }
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        adapter = BooksAdapter {
            presenter.openDetailsScreen(it)
        }
        recycler_view.apply {
            adapter = this@HomeFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(SpaceDecorator(16.dp2px))
            addItemDecoration(DividerItemDecoration(context, HORIZONTAL))
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = HomeFragment()

    }
}
