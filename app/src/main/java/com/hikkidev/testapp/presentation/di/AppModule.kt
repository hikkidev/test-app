/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.di

import android.content.Context
import com.hikkidev.testapp.data.storage.AppDatabase
import com.hikkidev.testapp.presentation.AppPresenter
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import toothpick.config.Module

class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)

        //Database
        bind(AppDatabase::class.java).toInstance(AppDatabase.getInstance(context))

        //Navigation
        bind(NavigationManager::class.java).singleton()
        bind(AppPresenter::class.java)
    }
}