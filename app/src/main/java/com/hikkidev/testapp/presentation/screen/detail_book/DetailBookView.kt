/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.detail_book

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkideveloper.core.mvp.view.BaseView


@StateStrategyType(AddToEndSingleStrategy::class)
interface DetailBookView : BaseView {
    fun showMessageAdded()

    fun showMessageUpdated()

    fun showMessageDeleted()

    fun hideDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setInfo(book: BookModel)

    fun setButtons(s: String)
}
