/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.home

import androidx.paging.PagedList
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkideveloper.core.mvp.view.BaseView


@StateStrategyType(AddToEndSingleStrategy::class)
interface HomeView : BaseView {

    fun setBooks(list: PagedList<BookModel>)

    fun progressBar(isEnable: Boolean)
}
