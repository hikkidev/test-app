/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.home.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import kotlinx.android.synthetic.main.item_book.view.*

class BookHolder(itemView: View, clickHandler: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {

    init {
        itemView.setOnClickListener { clickHandler(adapterPosition) }
    }

    fun bind(book: BookModel) {
        itemView.apply {
            text_view_title.text = book.title
            text_view_product_id.text =
                context.getString(R.string.template_product).format(book.productId)
            text_view_author.text =
                context.getString(R.string.template_author).format(book.author.first())
        }
    }
}