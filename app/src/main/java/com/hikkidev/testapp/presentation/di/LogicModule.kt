/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.di

import com.hikkidev.testapp.data.storage.BookGatewayImpl
import com.hikkidev.testapp.domain.entity.IdKeeper
import com.hikkidev.testapp.domain.entity.IdPublisher
import com.hikkidev.testapp.domain.gateways.BookGateway
import com.hikkidev.testapp.domain.implementation.IdHolder
import com.hikkidev.testapp.domain.provider.BookGatewayProvider
import com.hikkidev.testapp.domain.provider.IdKeeperProvider
import com.hikkidev.testapp.domain.provider.IdPublisherProvider
import toothpick.config.Module

class LogicModule : Module() {
    init {
        bind(BookGatewayImpl::class.java).singleton()
        bind(BookGateway::class.java).toProvider(BookGatewayProvider::class.java)
            .providesSingleton()

        bind(IdHolder::class.java).singleton()
        bind(IdKeeper::class.java).toProvider(IdKeeperProvider::class.java)
            .providesSingleton()
        bind(IdPublisher::class.java).toProvider(IdPublisherProvider::class.java)
            .providesSingleton()
    }
}