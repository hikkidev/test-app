/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.screen.detail_book

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.hikkidev.testapp.R
import com.hikkidev.testapp.data.storage.BookModel
import com.hikkidev.testapp.presentation.Screens
import com.hikkideveloper.core.mvp.LayoutId
import com.hikkideveloper.core.mvp.fragment.BaseFragment
import com.hikkideveloper.core.mvp.navigation.BackListener
import kotlinx.android.synthetic.main.fragment_detail_book.*
import toothpick.Scope
import toothpick.Toothpick


@LayoutId(R.layout.fragment_detail_book)
class DetailBookFragment : BaseFragment(), DetailBookView, BackListener {

    override val scope: Scope = Toothpick.openRootScope()

    private var alertDialog: AlertDialog? = null

    @InjectPresenter
    lateinit var presenter: DetailBookPresenter

    @ProvidePresenter
    fun providePresenter(): DetailBookPresenter = scope.getInstance(DetailBookPresenter::class.java)

    override fun onBack(): Boolean {
        presenter.back(Screens.APP_ROUTER)
        return true
    }

    override fun setInfo(book: BookModel) {
        text_product_id.setText(book.productId.toString())
        text_title.setText(book.title)
        text_author.setText(book.author.first())
    }

    override fun setButtons(s: String) {
        val button = MaterialButton(requireContext()).apply {
            text = context.getString(R.string.button_delete)
            setOnClickListener { presenter.removeBook() }
        }
        button_action.text = s
        container.addView(button, container.childCount - 1)
    }

    override fun showMessageDeleted() {
        alertDialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.dialog_title))
            .setMessage(getString(R.string.dialog_message_deleted))
            .setPositiveButton(getString(R.string.button_close)) { _, _ ->
                presenter.hideDialog()
                presenter.back(Screens.APP_ROUTER)
            }
            .setOnDismissListener { presenter.hideDialog() }
            .show()
    }

    override fun showMessageUpdated() {
        alertDialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.dialog_title))
            .setMessage(getString(R.string.dialog_message_updated))
            .setPositiveButton(getString(R.string.button_ok)) { _, _ ->
                presenter.hideDialog()
            }
            .setOnDismissListener { presenter.hideDialog() }
            .show()
    }


    override fun showMessageAdded() {
        alertDialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.dialog_title))
            .setMessage(getString(R.string.dialog_message_added))
            .setNegativeButton(getString(R.string.button_back)) { _, _ ->
                presenter.hideDialog()
                requireActivity().onBackPressed()
            }
            .setPositiveButton(getString(R.string.button_add)) { _, _ ->
                text_product_id.text?.clear()
                text_title.text?.clear()
                text_author.text?.clear()
                text_product_id.requestFocus()
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                )
            }
            .setOnDismissListener { presenter.hideDialog() }
            .show()
    }

    override fun hideDialog() {
        alertDialog?.dismiss()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_back.setOnClickListener { presenter.back(Screens.APP_ROUTER) }
        button_action.setOnClickListener { handleAction() }
    }

    private fun handleAction() {
        val id = text_product_id.text.toString()
        val title = text_title.text.toString()
        val author = text_author.text.toString()
        when {
            id.isEmpty() -> showToast(getString(R.string.error_empty_product_id))
            title.isEmpty() -> showToast(getString(R.string.error_empty_book_name))
            author.isEmpty() -> showToast(getString(R.string.error_empty_authros))
            else -> {
                hideKeyboard()
                presenter.handleAction(
                    BookModel(
                        productId = id.toLong(),
                        title = title,
                        author = listOf(author)
                    )
                )
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = DetailBookFragment()
    }
}
