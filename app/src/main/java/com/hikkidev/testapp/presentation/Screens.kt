/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation

import com.hikkidev.testapp.presentation.screen.detail_book.DetailBookFragment
import com.hikkidev.testapp.presentation.screen.home.HomeFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    const val APP_ROUTER = "application_router"

    object Home : SupportAppScreen() {
        override fun getFragment() = HomeFragment.newInstance()
        override fun getScreenKey(): String = "home_screen"
    }

    object DetailBook : SupportAppScreen() {
        override fun getFragment() = DetailBookFragment.newInstance()
        override fun getScreenKey(): String = "detail_book_screen"
    }
}