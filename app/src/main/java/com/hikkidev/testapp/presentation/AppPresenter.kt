/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation

import com.arellomobile.mvp.InjectViewState
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import com.hikkideveloper.core.mvp.presenter.BasePresenter
import com.hikkideveloper.core.mvp.view.BaseView
import javax.inject.Inject

@InjectViewState
class AppPresenter @Inject constructor(
    navigationManager: NavigationManager
) :
    BasePresenter<BaseView>(navigationManager) {

    override fun handleCoroutineException(throwable: Throwable) {}

    fun coldStart() {
        getRouter(Screens.APP_ROUTER).newRootScreen(Screens.Home)
    }
}