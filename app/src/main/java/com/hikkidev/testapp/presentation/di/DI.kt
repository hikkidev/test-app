/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.di

object DI {
    const val APP_SCOPE = "application_scope"
}
