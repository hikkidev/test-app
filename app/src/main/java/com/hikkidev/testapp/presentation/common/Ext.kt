/*
 * Developed by Sora Nai.
 * Copyright (c) 2019. All rights reserved.
 */

package com.hikkidev.testapp.presentation.common

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

inline fun View.makeVisibleIf(block: () -> Boolean) {
    if (visibility != View.VISIBLE && block()) {
        visibility = View.VISIBLE
    }
}

fun View.makeVisible() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.makeGone() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}


val Number.dp2px: Int
    get() = (this.toFloat() * Resources.getSystem().displayMetrics.density).toInt()